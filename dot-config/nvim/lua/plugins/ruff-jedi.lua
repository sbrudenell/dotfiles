return {
  {
    "neovim/nvim-lspconfig",
    ---@class PluginLspOpts
    opts = {
      ---@type lspconfig.options
      servers = {
        ruff = { mason = false },
        jedi_language_server = { mason = false },
      },
    },
  },
}
