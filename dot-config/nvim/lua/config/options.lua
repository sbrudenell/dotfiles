-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
-- https://github.com/LazyVim/LazyVim/discussions/5073#discussioncomment-11765474
vim.g.snacks_animate = false

vim.opt.relativenumber = false
